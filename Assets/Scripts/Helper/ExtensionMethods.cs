﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Object = UnityEngine.Object;

public static class ExtensionMethods
{
    public static int ParseFast(this string s)
    {
        // int r = 0;
        // for(var i = 0; i < s.Length; i++)
        // {
        //     char letter = s[i];
        //     r += System.Convert.ToInt32(letter);
        // }
        // return r;

        return s.Sum(System.Convert.ToInt32);
    }

    public static int ASCIISequenceToInt(this string s)
    {
        string intAsString = "";
        foreach(var letter in s)
        {
            string toAdd = System.Convert.ToInt32(letter).ToString();

            int diff = 3 - toAdd.Length;
            int currentExtender = 9;
            for(int a = 0; a < diff; a++)
            {
                intAsString += currentExtender.ToString();
                currentExtender--;
            }
            intAsString += toAdd;
        }

        int r = intAsString.ParseFast(); // just in case parse fails
        if(!int.TryParse(intAsString, out r))
        {
            Debug.LogError("Did you enter a seed with more than 3 letters. How dare you? ");
        }

        return r;
    }

    public static float Remap(this float f, float old1, float old2, float new1, float new2, bool clamp = false)
    {
        if(clamp)
        {
            if(old1 > old2)
                Extras.SwapFloat(ref old1, ref old2);

            Mathf.Clamp(f, old1, old2);
        }

        return new1 + (new2 - new1) * (f - old1) / (old2 - old1);
    }

    public static float GetRotation(this Vector2 vector)
    {
        return Mathf.Atan2(vector.y, vector.x) * Mathf.Rad2Deg - 90;
    }

    public static float GetRotation2D(this Transform transform)
    {
        return transform.eulerAngles.z;
    }

    public static void SetRotation2D(this Transform transform, float rotation)
    {
        Vector3 r = transform.eulerAngles;
        r.z = rotation;
        transform.eulerAngles = r;
    }

    public static float LookRotation2D(this Transform t, Vector2 direction)
    {
        return Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90;
    }

    public static float LookRotation2D(this Transform t, Vector3 targetPosition)
    {
        Vector2 v = targetPosition - t.position;
        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg - 90;
    }

    public static float LookRotation2D(this Transform t, Transform target)
    {
        if(target == null)
            return t.rotation.eulerAngles.z;

        Vector2 v = target.transform.position - t.position;
        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg - 90;
    }

    public static Vector2 PerpendicularClockwise(this Vector2 v)
    {
        return new Vector2(v.y, -v.x);
    }

    public static Vector2 PerpendicularCounterClockwise(this Vector2 vector2)
    {
        return new Vector2(-vector2.y, vector2.x);
    }

    public static void RotateTowards(this Rigidbody2D rb, float targetRotation, float maxDegreesDelta)
    {
        rb.MoveRotation(Mathf.MoveTowardsAngle(rb.transform.eulerAngles.z, targetRotation, maxDegreesDelta));
    }

    public static void RotateTowards(this Transform t, float targetRotation, float maxDegreesDelta)
    {
        t.SetRotation2D(Mathf.MoveTowardsAngle(t.eulerAngles.z, targetRotation, maxDegreesDelta));
    }

    public static void AddSpacesToSentence(this string text, bool preserveAcronyms)
    {
        if(string.IsNullOrWhiteSpace(text))
            return;
        StringBuilder newText = new StringBuilder(text.Length * 2);
        newText.Append(text[0]);
        for(int i = 1; i < text.Length; i++)
        {
            if(char.IsUpper(text[i]))
                if((text[i - 1] != ' ' && !char.IsUpper(text[i - 1])) ||
                    (preserveAcronyms && char.IsUpper(text[i - 1]) &&
                        i < text.Length - 1 && !char.IsUpper(text[i + 1])))
                    newText.Append(' ');
            newText.Append(text[i]);
        }
        text = newText.ToString();
    }

    public static void DestroyAllChildren(this Transform t)
    {
        int childCount = t.childCount;
        for(int i = 0; i < childCount; i++)
        {
            GameObject child = t.GetChild(i).gameObject;
            Object.Destroy(child);
        }
    }

    public static IList<T> Clone<T>(this IList<T> listToClone) where T : ICloneable
    {
        return listToClone.Select(item => (T) item.Clone()).ToList();
    }
}