﻿public static class Constants
{
    public struct Tags
    {
        public const string Player = "Player";
        public const string Enemy = "Enemy";
    }

    public struct Input
    {
        public const string Horizontal = "Horizontal";
        public const string Vertical = "Vertical";
        public const string Fire = "Fire";
        public const string Boost = "Boost";
        public const string SpecialFire = "Special Fire";
        public const string InspectToggle = "Inspect Toggle";
    }

    public struct ControlSchemes
    {
        public const string Gamepad = "Gamepad";
        public const string KeyboardMouse = "KeyboardMouse";
    }
}
