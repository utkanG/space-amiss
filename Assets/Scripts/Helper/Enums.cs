﻿public enum Layer
{
    UI = 5,
    Neutral = 8,
    EnemySmall = 9,
    EnemyLarge = 10,
    PlayerHull = 11,
    PlayerFire = 12,
    EnemyFire = 13,
    Biomatter = 14,
    Structure = 15,
    ZLevel = 16,
    Sensor = 17,
    LaserGate = 18,
    SectorMap = 19
}

public enum DamageEffect
{
    NoDamage,
    PlayerShip,
    DebrisSmall,
    DebrisLarge,
    DebrisEnergetic,
    DebrisLaserGate
}

public enum ExplosionEffect
{
    NoExplosion,
    Tiny,
    Small,
    Medium,
    Large,
    Missile1Explosive
}

public enum CelestialBody
{
    Planet,
    Star,
    BlackHole,
    Moon
}

public enum PlanetType
{
    Temperate,
    Ocean,
    Barren,
    Icy,
    Lava,
    Gas
}

public enum StarType
{
    RedDwarf,
    YellowDwarf,
    BrownDwarf,
    WhiteDwarf,
    BlueGiant,
    WhiteGiant,
    OrangeGiant,
    RedGiant,
    NeutronStar,
    Protostar
}

public enum GameState
{
    None,
    Region,
    Sector
}

public enum Orbit
{
    Low,
    High
}

public enum HumanPresence
{
    None,
    Low,
    Moderate,
    High,
    Unsafe
}

public enum FieldDensity
{
    None,
    Sparse,
    Dense
}

public enum HumanTech
{
    None,
    Basic,
    Developed,
    Advanced
}

public enum RegionRelation
{
    Same,
    Adjacent,
    Far
}

public enum HumanAdvancedTech
{
    None,

    // advanced orbital base subtech
    StarGate,
    PlanetBuster,
    IntergalacticShip,
    DysonConstruction,

    // advanced colony subtech
    PlanetMine,
    MassFabrication,
    Ecumenopolis
}

// not used for now, originally for stellar flares etc.
public enum Intesity
{
    Weak,
    Normal,
    Powerful
}

public enum AIBrainState
{
    Dead,
    Approach,
    Engage,
    // other states maybe,
    // EMPed
    // Chaos, everything is random
    // CrewDead, only weapons update
}