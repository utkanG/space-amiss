﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D)), RequireComponent(typeof(Collider2D))]
public class PhysicsGrid : MonoBehaviour
{
    public bool StartOn;

    private bool gravityOn;
    public bool GravityOn
    {
        get => gravityOn;
        set
        {
            gravityOn = value;
            ToggleEntityGravity();
        }
    }

    public Rigidbody2D Rigidbody { get; private set; }

    private HashSet<PhysicsEntity> physicsEntities;

    private void Awake()
    {
        Rigidbody = GetComponent<Rigidbody2D>();

        physicsEntities = new HashSet<PhysicsEntity>();

        // TODO Get all entities inside and toggle their gravities accordingly
    }

    private void Start()
    {
        GravityOn = StartOn;
    }

    private bool TryAddEntity(PhysicsEntity entity)
    {
        return physicsEntities.Add(entity);
    }

    private bool TryRemoveEntity(PhysicsEntity entity)
    {
        return physicsEntities.Remove(entity);
    }

    [ContextMenu("Toggle Gravity")]
    public void ToggleGravity()
    {
        GravityOn = !GravityOn;
    }

    private void FixedUpdate()
    {
        // Temp
        if(Input.GetKey(KeyCode.RightArrow))
        {
            Vector2 velocityDelta = Vector2.right * (Time.deltaTime * 25);
            Rigidbody.velocity += velocityDelta;
        }
        if(Input.GetKey(KeyCode.LeftArrow))
        {
            Vector2 velocityDelta = Vector2.left * (Time.deltaTime * 25);
            Rigidbody.velocity += velocityDelta;
        }
        if(Input.GetKey(KeyCode.UpArrow))
        {
            Rigidbody.angularVelocity += (Time.deltaTime * 120);
        }
        if(Input.GetKey(KeyCode.DownArrow))
        {
            Rigidbody.angularVelocity -= (Time.deltaTime * 120);
        }
        if(Input.GetKey(KeyCode.R))
        {
            Rigidbody.angularVelocity = 0;
        }
        if(Input.GetKey(KeyCode.T))
        {
            Rigidbody.velocity = Vector2.zero;
        }

        if (!gravityOn) return;

        foreach(PhysicsEntity entity in physicsEntities)
        {
            Vector3 gridPivot = transform.position;
            Quaternion deltaQuaternion = Quaternion.AngleAxis(Rigidbody.angularVelocity * Time.deltaTime * entity.GripCoefficient, Vector3.forward);

            entity.transform.rotation *= deltaQuaternion;
                    
            Vector2 toOrigin = entity.transform.position - gridPivot;
            // calculate the tangential velocity, both from rotation and the force to keep the object from being pushed outwards (I think)
            Vector2 tangentialVelocity = toOrigin.PerpendicularCounterClockwise().normalized * (toOrigin.magnitude * Rigidbody.angularVelocity * Mathf.Deg2Rad);
            tangentialVelocity -= toOrigin.normalized * (tangentialVelocity.sqrMagnitude / toOrigin.magnitude* Time.deltaTime / 2);

            if (entity.Latched) // also when not in gravity
            {
                entity.transform.position = Extras.RotatePointAroundPivot(entity.transform.position, gridPivot, deltaQuaternion);
            }

            entity.UpdateVelocity(Rigidbody.velocity, tangentialVelocity * .995f);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        PhysicsEntity entityToAdd = other.GetComponentInParent<PhysicsEntity>();
        if(TryAddEntity(entityToAdd))
        {
            entityToAdd.InGravity = gravityOn;
            entityToAdd.OnGridEnter(this);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        PhysicsEntity entityToRemove = other.GetComponentInParent<PhysicsEntity>();
        if(TryRemoveEntity(entityToRemove))
        {
            entityToRemove.InGravity = false;
            entityToRemove.OnGridExit();
        }
    }

    private void ToggleEntityGravity()
    {
        if(physicsEntities == null) return; 

        foreach(PhysicsEntity entity in physicsEntities)
        {
            entity.InGravity = gravityOn;
        }
    }
}
