﻿using UnityEditor;

[CustomEditor(typeof(PhysicsGrid))]
public class PhysicsGridEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        PhysicsGrid activePhysicsGrid = (PhysicsGrid) target;

        activePhysicsGrid.GravityOn = EditorGUILayout.Toggle("Gravity On", activePhysicsGrid.GravityOn);
    }
}
