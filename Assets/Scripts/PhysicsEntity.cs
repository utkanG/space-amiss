﻿using UnityEngine;
using UnityEngine.Events;

public delegate void VoidDelegate();
public delegate void PhysicsGridDelegate(PhysicsGrid grid);

[RequireComponent(typeof(Rigidbody2D)), RequireComponent(typeof(Collider2D))]
public class PhysicsEntity : MonoBehaviour
{
    private Rigidbody2D Rigidbody { get; set; }

    public bool InGravity;
    public float GripRecovery = 1; // How fast the object recovers its gripCoefficient.

    public bool Latched { get; private set; } // Forces the entity to act as if it is parented to grid, TO PROPERTY. Toggled via entity owner.
    public float GripCoefficient { get; private set; } // 0 to 1: 1 means the velocity is fully transferred. Set lower than 0 to delay grip recovery.

    // How much delta-V square is needed to make this entity lose its friction (if not latched). Depletes GridCoefficient.
    public float MaxDeltaV = 10;

    public Vector2 SourceVelocity { get; private set; }

    public Vector2 Velocity => Rigidbody.velocity;

    public VoidDelegate GripLostEvent;
    public PhysicsGridDelegate ActiveGridChangeEvent;

    private void Awake()
    {
        Rigidbody = GetComponent<Rigidbody2D>();
    }

    public void UpdateVelocity(Vector2 axialVelocity, Vector2 tangentialVelocity)
    {
        //Debug.Log(gameObject.name + ":" + axialVelocity.magnitude + ":" + tangentialVelocity.magnitude);
        Vector2 totalVelocity = axialVelocity + tangentialVelocity;
        float deltaTotal = (totalVelocity - Rigidbody.velocity).magnitude;
        float exceededDeltaV = deltaTotal - MaxDeltaV;

        if(exceededDeltaV >= 0 && !Latched)
        {
            // disable entity control ?
            //Rigidbody.velocity = Rigidbody.velocity * Mathf.Clamp01(exceededDeltaV / MaxDeltaVSqr) + tangentialVelocity;

            GripLostEvent?.Invoke();
            GripCoefficient = 0;
        }

        // Entity tries to grip, matching the grid velocity over time. When the change is smaller, grip starts to recover
        GripCoefficient = Mathf.MoveTowards(GripCoefficient, 1, Time.deltaTime * (GripCoefficient <= 0 ? 5 : GripRecovery)); // maybe periodically increase this? 

        Vector2 targetVelocity = axialVelocity + (Latched ? Vector2.zero : tangentialVelocity + SourceVelocity);
        Rigidbody.velocity = Vector2.MoveTowards(Rigidbody.velocity, targetVelocity, (Rigidbody.velocity - targetVelocity).magnitude * Mathf.Clamp01(GripCoefficient));
    }

    /// <summary>
    /// Used to modify the local velocity of the entity.
    /// </summary>
    /// <param name="velocity"> New velocity </param>
    public void SetSourceVelocity(Vector2 velocity)
    {
        SourceVelocity = velocity;

        if (!InGravity) // since a grid does not update main body
        {
            Rigidbody.velocity = SourceVelocity;
        }
    }

    public void OnGridEnter(PhysicsGrid grid)
    {
        // to simulate the friction of an object
        Rigidbody.angularDrag = 10000;

        SourceVelocity = Vector2.zero;

        GripCoefficient = -.5f;

        ActiveGridChangeEvent?.Invoke(grid);
    }

    public void OnGridExit()
    {
        GripCoefficient = 0;
        Rigidbody.angularDrag = 0;

        SourceVelocity = Rigidbody.velocity;
        Rigidbody.velocity = SourceVelocity;

        ActiveGridChangeEvent?.Invoke(null);
    }

    public void ToggleLatch(bool toggle)
    {
        Latched = toggle;
        Rigidbody.isKinematic = toggle;
    }

    private void OnDrawGizmosSelected()
    {
        if(!Application.isPlaying) return;

        Debug.DrawRay(transform.position, Velocity, Color.yellow / 4);
        Debug.DrawRay(transform.position, SourceVelocity, Color.green / 3);
    }
}