using UnityEngine;

public class CameraShake : MonoBehaviour
{
    [SerializeField] private float _frequency = 25;
    [SerializeField] private float _traumaPower = 2;
    [SerializeField] private Vector3 _maximumTranslationShake;
    [SerializeField] private Vector3 _maximumAngularShake;

    private float _recoverySpeed = 1.5f;
    private float _trauma = 0;
    private float _seed;
    private Vector3 originalPosition;
    private Vector3 originalEulerRotation;

    private void Awake()
    {
        _seed = Random.value;
    }

    private void Update()
    {
        if(_trauma == 0) return;

        float shake = Mathf.Pow(_trauma, _traumaPower);
        transform.position = new Vector3(
            _maximumTranslationShake.x * (Mathf.PerlinNoise(_seed, Time.time * _frequency) * 2 - 1),
            _maximumTranslationShake.y * (Mathf.PerlinNoise(_seed + 1, Time.time * _frequency) * 2 - 1),
            _maximumTranslationShake.z * (Mathf.PerlinNoise(_seed + 2, Time.time * _frequency) * 2 - 1)
            ) * shake + originalPosition;
        transform.rotation = Quaternion.Euler(new Vector3(
            _maximumAngularShake.x * (Mathf.PerlinNoise(_seed, Time.time * _frequency) * 2 - 1),
            _maximumAngularShake.y * (Mathf.PerlinNoise(_seed + 1, Time.time * _frequency) * 2 - 1),
            _maximumAngularShake.z * (Mathf.PerlinNoise(_seed + 2, Time.time * _frequency) * 2 - 1)
            ) * shake + originalEulerRotation);
        _trauma = Mathf.Clamp01(_trauma - _recoverySpeed * Time.deltaTime);
    }

    public void InduceStress(float stress, float recoverySpeed = 1.5f)
    {
        if(_trauma == 0)
        {
            originalPosition = transform.position;
            originalEulerRotation = transform.rotation.eulerAngles;
        }

        _trauma = Mathf.Clamp01(_trauma + stress);
        _recoverySpeed = recoverySpeed;
    }
}