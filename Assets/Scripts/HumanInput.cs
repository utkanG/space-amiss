﻿using UnityEngine;
using UnityEngine.InputSystem;

public class HumanInput : MonoBehaviour
{
    public Human PlayerHuman;

    private InputMaster controls;

    // required for pointing rays from screen
    private Camera mainCamera;
    private Transform mainCameraTransform;
    private Plane xyPlaneMain;

    private PlayerInput playerInput; // to control the switch between a keyboard/mouse & controller

    private void Awake()
    {
        controls = new InputMaster();
        mainCamera = Camera.main;
        playerInput = FindObjectOfType<PlayerInput>();
        if (mainCamera != null) mainCameraTransform = mainCamera.transform;

        xyPlaneMain = new Plane(Vector3.forward, Vector3.zero);
    }

    private void OnEnable()
    {
        controls.Enable();
    }

    private void OnDisable()
    {
        controls.Disable();
    }

    private void Update()
    {
        switch (playerInput.currentControlScheme)
        {
            case Constants.ControlSchemes.Gamepad:
            {
                if(controls.Player.MoveToOrientation.ReadValue<float>() > 0)
                {
                    Vector2 leftStickInput = mainCameraTransform.TransformDirection(Gamepad.current.leftStick.ReadValue());

                    PlayerHuman.SetLookDirection(leftStickInput);
                }
                else
                {
                    Vector2 rightStickInput = mainCameraTransform.TransformDirection(Gamepad.current.rightStick.ReadValue());

                    if(!Extras.CompareVector2D(Vector2.zero, rightStickInput, .25f)) // (.25f is the dead zone for orientation so the human stops turning)
                        PlayerHuman.SetLookDirection(rightStickInput);
                    else
                        PlayerHuman.SetLookDirection(PlayerHuman.transform.up);
                }

                PlayerHuman.SetMoveDirection(controls.Player.BasicMovement.ReadValue<Vector2>());
                break;
            }
            case Constants.ControlSchemes.KeyboardMouse:
            {
                Vector2 mousePosition = Mouse.current.position.ReadValue();
                Vector2 mouseWorldPosRelativeToPlayer = GetWorldPositionFrom(mousePosition) - (Vector2) PlayerHuman.transform.position;

                PlayerHuman.SetLookDirection(mouseWorldPosRelativeToPlayer);

                if(controls.Player.MoveToOrientation.ReadValue<float>() > 0)
                {
                    PlayerHuman.SetMoveDirection(mouseWorldPosRelativeToPlayer);
                }
                else
                {
                    PlayerHuman.SetMoveDirection(mainCameraTransform.TransformDirection(controls.Player.BasicMovement.ReadValue<Vector2>()));
                }

                break;
            }
        }
    }

    private Vector2 GetWorldPositionFrom(Vector2 screenPosition)
    {
        Ray ray = mainCamera.ScreenPointToRay(screenPosition);
        xyPlaneMain.Raycast(ray, out float distance);
        return ray.GetPoint(distance);
    }
}
