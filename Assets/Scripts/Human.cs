﻿using UnityEngine;

public class Human : MonoBehaviour
{
    [Header("Movement - Suit Settings")]
    /*
     * Suit movement will depend on equipment.
     * TODO It would be nice to have these values stored inside a default human data
     */
    public float SuitAcceleration = 15;
    public float SuitRotationCoefficient = 2;

    [Header("Movement - Leg Settings")]
    /*
     * Leg movement values will depend on the health status and human equipment.
     * TODO It would be nice to have these values stored inside a default human data
     */
    public float LegAcceleration = 8;
    public float LegMaxSpeed = 5;
    public float LegRotationCoefficient = 8;

    private Vector2 moveDirection;
    private Vector2 lookDirection;

    /*
     * Enums may be required for different states, like health and movement status.
     * Combine it with the entity gravity for complex results.
     */

    private PhysicsEntity physicsEntity;
    private Rigidbody2D mainRigidbody;
    private Vector2 velocity;

    private void Awake()
    {
        physicsEntity = GetComponent<PhysicsEntity>();
        mainRigidbody = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        float lookAngleDelta = Mathf.DeltaAngle(transform.GetRotation2D(), transform.LookRotation2D(lookDirection));

        if(physicsEntity.InGravity)
        {
            // when inside a grid, the controlled velocity is separate from the combined velocity which is source
            velocity = physicsEntity.SourceVelocity;

            float velocityAngleDelta = Mathf.Abs(Mathf.DeltaAngle(transform.GetRotation2D(), velocity.GetRotation()));
            float orientationMultiplier = velocityAngleDelta.Remap(15, 180, 1f, .75f); // (if the difference is lower than the first value, the player will run at max speed)

            velocity = Vector2.MoveTowards(velocity, moveDirection * (LegMaxSpeed * orientationMultiplier),
                LegAcceleration * Time.deltaTime); // (Decelerate slower if moving too fast)

            mainRigidbody.angularVelocity += lookAngleDelta * LegRotationCoefficient * Time.deltaTime * 10000;
        }
        else
        {
            velocity = physicsEntity.Velocity;
            velocity += moveDirection * (SuitAcceleration * Time.deltaTime);

            mainRigidbody.angularDrag = 10;
            mainRigidbody.angularVelocity += lookAngleDelta * SuitRotationCoefficient * Time.deltaTime * 10;
        }

        physicsEntity.SetSourceVelocity(velocity);
    }

    public void SetMoveDirection(Vector2 moveDir)
    {
        moveDirection = moveDir.normalized;
    }

    public void SetLookDirection(Vector2 lookDir)
    {
        lookDirection = lookDir.normalized;
    }

    private void OnDrawGizmosSelected()
    {
        if(!Application.isPlaying) return;

        Debug.DrawRay(transform.position, moveDirection, Color.cyan / 1.5f);

        Gizmos.color = Color.white;
        Gizmos.DrawSphere(transform.position + (Vector3) lookDirection, .1f);
    }
}
