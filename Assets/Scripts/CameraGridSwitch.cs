﻿using UnityEngine;

public class CameraGridSwitch : MonoBehaviour
{
    public float RotationBlendSpeed = 1;
    public AnimationCurve RotationBlendSpeedOverTime;

    private float curveTime = 0;

    private PhysicsEntity targetEntity;
    private PhysicsGrid activeGrid;

    private bool activeGridPresent;
    private bool lockedToGrid;

    private Quaternion targetRotation;

    private void Awake()
    {
        targetEntity = FindObjectOfType<HumanInput>().PlayerHuman.GetComponent<PhysicsEntity>();
    }

    private void Start()
    {
        targetEntity.ActiveGridChangeEvent += OnGridChange;
    }

    private void OnDestroy()
    {
        // ReSharper disable once DelegateSubtraction
        targetEntity.ActiveGridChangeEvent -= OnGridChange;
    }

    // Late or Fix Who Knows?
    private void LateUpdate()
    {
        if(lockedToGrid) return;

        float currentBlendSpeed = RotationBlendSpeed * RotationBlendSpeedOverTime.Evaluate(curveTime);

        if(activeGridPresent)
        {
            targetRotation = activeGrid.transform.rotation; // needs to be refreshed as the active grid rotates
            transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation,Time.deltaTime * (Mathf.Abs(activeGrid.Rigidbody.angularVelocity) + currentBlendSpeed));
        }
        else
            transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, Time.deltaTime * currentBlendSpeed);

        float angleDifference = Mathf.Abs(Mathf.DeltaAngle(transform.GetRotation2D(), targetRotation.eulerAngles.z));
        curveTime = Extras.Remap(angleDifference, 0, 180, 1, 0, true);

        if(angleDifference < .1f)
        {
            lockedToGrid = true;
            if(activeGridPresent)
                transform.parent = activeGrid.transform;
        }
    }

    public void OnGridChange(PhysicsGrid newPhysicsGrid)
    {
        curveTime = 0;
        transform.parent = null;

        activeGridPresent = newPhysicsGrid != null;
        activeGrid = newPhysicsGrid;

        lockedToGrid = false;

        targetRotation = Quaternion.identity;
        if(activeGridPresent)
            targetRotation = activeGrid.transform.rotation;
    }
}